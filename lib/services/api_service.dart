import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:real_toonflic/models/webtoon_model.dart';

class ApiService {
  static String baseUrl = "https:///webtoon-crawler.nomadcoders.workers.dev";
  static String today = "today";

  static Future<List<WebtoonModel>> getTodaysToons() async {
    List<WebtoonModel> webtoonInstances = [];
    //pub.dev에서 http패키지 불러오기
    final url = Uri.parse('$baseUrl/$today');
    //서버의 네트워크 환경과
    //유저의 메모리 환경의 문제로 시간이 걸릴 수 있다.
    //요청이 처리될 때까지 기다려야한다.
    //dart에게 이 작업이 끝날 때까지 기다리라고 하고 싶다면.
    //async를 추가.하는 것이 하나의 룰이다.
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final List<dynamic> webtoons = jsonDecode(response.body);
      for (var webtoon in webtoons) {
        webtoonInstances.add(WebtoonModel.fromJson(webtoon));
      }
      return webtoonInstances;
    }
    throw Error();
  }
}
